<?php
/**
 * @author      Jean-Baptiste Verrey<jeanbaptiste.verrey@gmail.com>
 * @copyright   2012 Jean-Baptiste Verrey
 * @license     see license.txt
 * @since       1.0
 */
namespace HappyAOP {
        class Compiler {

                private static function _readDir($path,&$files = array(),$hookSuffix = 'hook') {
                        if ($handle = opendir($path)) {
                                while (false !== ($entry = readdir($handle))) {
                                        if($entry=='.' || $entry=='..')continue;
                                        if(is_dir($path.$entry))
                                                self::_readDir ($path.$entry.'/',$files);
                                        else if(strpos($entry,'.'.$hookSuffix.'.php')!==false){
                                                $files[] =  $path.str_replace('.'.$hookSuffix.'.php','.php',$entry);
                                        }
                                }
                                closedir($handle);
                        }
                }

                public static function compileFolder($path = '',$absolutePathToApplicationFolder='',$aopSuffix = 'original',$hookSuffix = 'hook') {
                        $files = array();
                        self::_readDir($path,$files,$aopSuffix,$hookSuffix);
                        $compiler = new \HappyAOP\Compiler();
                        $compiler->compile($files,$absolutePathToApplicationFolder,$aopSuffix,$hookSuffix);
                }
                
                public static function uncompileFolder($path = '',$absolutePathToApplicationFolder='',$aopSuffix = 'original',$hookSuffix = 'hook') {
                        $files = array();
                        self::_readDir($path,$files,$aopSuffix,$hookSuffix);
                        foreach($files as $file){
                                $realFile = str_replace('.php','.'.$aopSuffix.'.php',$file);
                                if(file_exists($absolutePathToApplicationFolder.$realFile)){
                                        rename($absolutePathToApplicationFolder.$realFile,$absolutePathToApplicationFolder.$file);
                                }
                        }
                }

                public function compile($aspects, $absolutePathToApplicationFolder, $aopSuffix = 'original', $hookSuffix = 'hook', $addRequire = true, $log = true) {
                        foreach ($aspects as $aspect) {
                                if ($log)
                                        echo $aspect . ': ';
                                if (!is_array($aspect)) {
                                        $aspect = array($absolutePathToApplicationFolder => $aspect);
                                }
                                $folder = key($aspect);
                                $file = current($aspect);

                                // if an AOP file exists we use it (it means the original.php is the CACHED version)
                                $aopFile = str_replace('.php', '.' . $aopSuffix . '.php', $file);
                                $aspectFile = str_replace('.php', '.' . $hookSuffix . '.php', $file);
                                if (file_exists($folder . $aopFile)) {
                                        $hasAopFile = true;
                                        $source = file_get_contents($folder . $aopFile);
                                } else {
                                        $source = file_get_contents($folder . $file);
                                        $hasAopFile = false;
                                }
                                // load info from aspect class
                                require($folder . $aspectFile);
                                $classesLoaded = get_declared_classes();
                                $class = $classesLoaded[count($classesLoaded) - 1];
                                $reflection = new \ReflectionClass($class);
                                $hasMethods = $methods = array();

                                foreach ($reflection->getMethods() as $methodObject) {
                                        $hasMethods[$methodObject->name] = true;
                                        $methods[str_replace(array('before_', 'after_'), '', $methodObject->name)] = true;
                                }

                                $modified = false;

                                foreach ($methods as $method => $unused) {
                                        if (preg_match('|(function\s*' . $method . '\s*\(([^\)]*)\))\s*{|', $source, $functionInfo)) {

                                                $pos = strpos($source, $functionInfo[1]);
                                                if ($pos !== false) {

                                                        // remove all the initialisation or class hint from call
                                                        $ex = explode(',', $functionInfo[2]);
                                                        $args = array();
                                                        foreach ($ex as $v) {
                                                                if (preg_match('|([^\$]*)?(\$[^=]*)|', $v, $match)) {
                                                                        $args[] = $match[2];
                                                                }
                                                        }
                                                        // INJECT BEFORE
                                                        if (isset($hasMethods['before_' . $method]) || isset($hasMethods[$method])) {
                                                                if ($log)
                                                                        echo ' before';
                                                                $call = PHP_EOL . '// HappyAOP - Automatic injection  ' . PHP_EOL . 'if(class_exists(\'\\\\' . str_replace('\\', '\\\\', $class) . '\')){$injectedAspectClassObject = new \\' . $class . '($this); ' . '$injectedAspectClassObject->' . (isset($hasMethods['before_' . $method]) ? 'before_' : '') . $method . '(' . implode(',', $args) . ');}' . '// /HappyAOP' . PHP_EOL;

                                                                $sourceTruncated = substr($source, $pos + strlen($functionInfo[0]));
                                                                $source = substr($source, 0, $pos) . $functionInfo[0] . $call;
                                                        } else {
                                                                $sourceTruncated = $source;
                                                        }



                                                        // INJECT BEFORE
                                                        if (isset($hasMethods['after_' . $method])) {
                                                                if ($log)
                                                                        echo ' after';
                                                                $copySourceTruncated = $sourceTruncated;
                                                                $openAcollade = 1;
                                                                do {
                                                                        $posOpeningAccolade = strpos($copySourceTruncated, '{');
                                                                        $posClosingAccolade = strpos($copySourceTruncated, '}');
                                                                        if ($posOpeningAccolade !== false && $posOpeningAccolade < $posClosingAccolade) {
                                                                                $openAcollade++;
                                                                                $copySourceTruncated = substr($copySourceTruncated, $posOpeningAccolade);
                                                                        } else if ($posOpeningAccolade === false && $posClosingAccolade === false) {
                                                                                $openAcollade = 0;
                                                                                throw new \Exception('class malformed, cannot find end of function');
                                                                        } else {
                                                                                $openAcollade--;
                                                                                $copySourceTruncated = substr($copySourceTruncated, $posClosingAccolade);
                                                                        }
                                                                } while ($openAcollade > 0);

                                                                $call = PHP_EOL . '// HappyAOP - Automatic injection  ' . PHP_EOL . 'if(class_exists(\'\\\\' . str_replace('\\', '\\\\', $class) . '\')){$injectedAspectClassObject = new \\' . $class . '($this); ' . '$injectedAspectClassObject->after_' . $method . '(' . implode(',', $args) . ');}' . '// /HappyAOP' . PHP_EOL;

                                                                $source.= substr($sourceTruncated, 0, $posClosingAccolade) . $call . substr($sourceTruncated, $posClosingAccolade);
                                                        } else {
                                                                $source.= $sourceTruncated;
                                                        }
                                                        $modified = true;
                                                }
                                        }
                                }

                                if ($modified) {
                                        // WRITE SOURCE
                                        // rename original to aop file if it does not exists
                                        if (!$hasAopFile) {
                                                echo ' AOP:created';
                                                rename($folder . $file, $folder . $aopFile);
                                        }

                                        // write in the cached file 
                                        $disclaimer = PHP_EOL . '/**' . PHP_EOL
                                                . ' * @aop_version:' . time() . PHP_EOL
                                                . ' * @original_file:' . $folder . $aopFile . PHP_EOL
                                                . ' * ' . PHP_EOL
                                                . ' * ' . PHP_EOL
                                                . ' * ' . PHP_EOL
                                                . ' * THIS IS A CACHED HAPPYAOP FILE, changes in that file will be crushed by HappyAOP when compiling' . PHP_EOL
                                                . ' * ' . PHP_EOL
                                                . ' * ' . PHP_EOL
                                                . ' * ' . PHP_EOL
                                                . '**/' . PHP_EOL;

                                        if ($addRequire) { // inject after namespace
                                                $pos = strpos($source, 'namespace');
                                                $require = '// HappyAOP - Automatic injection' . PHP_EOL . 'require __DIR__.\'/' . basename($aspectFile) . '\';' . PHP_EOL;
                                                if ($pos !== false) {
                                                        $pos2 = strpos($source, '{', $pos);
                                                        if ($pos2 !== false) {
                                                                $source = substr($source, 0, $pos2 + 1) . $require . substr($source, $pos2 + 1);
                                                        }else
                                                                throw new \Exception('namespace is not opened properly by bracket');
                                                }else // after the class
                                                        $disclaimer = '<?php ' . $disclaimer . '?>' . $source . $require;
                                        }

                                        echo ' OK' . PHP_EOL;
                                        file_put_contents($folder . $file, '<?php ' . $disclaimer . '?>' . $source);
                                } else {
                                        echo ' UNMODIFIED' . PHP_EOL;
                                }
                        }
                }
        }
}


